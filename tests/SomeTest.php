<?php

use Last1971\SomeTest\SomeClass;
use PHPUnit\Framework\TestCase;

class SomeTest extends TestCase
{
    private SomeClass $someClass;

    public function setUp(): void
    {
        parent::setUp();
        $this->someClass = new SomeClass();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->someClass);
    }

    public function testHello()
    {
        $this->assertEquals('Hello', 'Hel' . 'lo');
    }

    public function testFirst()
    {
        $this->assertEquals([], $this->someClass->solve(1, 1, 0));
    }

    public function testSecond()
    {
        $this->assertCount(2, $this->someClass->solve(2, 2, 0));
    }
}