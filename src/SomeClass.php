<?php

namespace Last1971\SomeTest;

class SomeClass
{
    public function doSomething()
    {
        echo 'I do something' . PHP_EOL;
    }

    public function solve(float $a, float $b, float $c): array
    {
        return [];
    }
}